
package Reportes;

import Modelo.Clientes;
import Modelo.Productos;
import java.io.FileOutputStream;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;


public class ReporteExcel {
    public ReporteExcel(){
        
    }
    
    public Boolean crearReporteClientes(){
        Clientes cliente = new Clientes();
        ArrayList<Clientes> listClientes = cliente.BuscarTodos();
        
        //Crear libro
        Workbook libro = new HSSFWorkbook();
        //Crear hoja
        Sheet hoja = libro.createSheet("ReporteClientes");
        //Añadir los datos en las celdas
        int i = 0; //Manejar las filas de la hoja
        for(Clientes c: listClientes){
            Row fila = hoja.createRow(i);
            fila.createCell(0).setCellValue(c.getId());
            fila.createCell(1).setCellValue(c.getNombre());
            fila.createCell(2).setCellValue(c.getApellido());
            fila.createCell(3).setCellValue(c.getTelefono());
            i++;
        }
        
        try {
            String ruta = System.getProperty("user.home");
            
            FileOutputStream salida = new FileOutputStream(ruta+"/Descargas/ReporteClientes2021.xls");
            libro.write(salida);
            salida.close();
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    } 
    
    public Boolean crearReporteProductos(){
        Productos producto = new Productos();
        ArrayList<Productos> listProductos = producto.BuscarTodos();
        
        //Crear libro
        Workbook libro = new HSSFWorkbook();
        //Crear hoja
        Sheet hoja = libro.createSheet("ReporteProductos");
        //Añadir los datos en las celdas
        int i = 0; //Manejar las filas de la hoja
        for(Productos p: listProductos){
            Row fila = hoja.createRow(i);
            fila.createCell(0).setCellValue(p.getId());
            fila.createCell(1).setCellValue(p.getNombre());
            fila.createCell(2).setCellValue(p.getCantidad());
            fila.createCell(3).setCellValue(p.getPrecio());
            i++;
        }
        
        try {
            String ruta = System.getProperty("user.home");
            
            FileOutputStream salida = new FileOutputStream(ruta+"/Descargas/ReporteProductos2021.xls");
            libro.write(salida);
            salida.close();
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    } 
}
