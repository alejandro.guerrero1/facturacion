package Conexion;
import com.mysql.cj.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
    private String dbUrl = "jdbc:mysql://localhost:3306/facturasdb";
    private String user = "root";
    private String pwd = "";
    
    public Conexion (){
    }
    
    public Connection Conectar(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(dbUrl,user,pwd);
            return conn;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            return conn;
        }
    }
    
    public void Cerrar(Connection conn){
        try{
            if(conn != null){
                conn.close();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
