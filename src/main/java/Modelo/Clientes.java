package Modelo;

import Conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Clientes {
    private int id;
    private String nombre;
    private String apellido;
    private String telefono;
    
    public Clientes(){
        
    }

    public Clientes(int id, String nombre, String apellido, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public Boolean Insertar(){
        Conexion con = new Conexion();
        String sql ="INSERT INTO Clientes(Id, Nombre, Apellido, Telefono) VALUES (null,?,?,?);";
        try {
            PreparedStatement pst = con.Conectar().prepareStatement(sql);
            pst.setString(1, getNombre());
            pst.setString(2, getApellido());
            pst.setString(3, getTelefono());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public Boolean Actualizar(){
        Conexion con = new Conexion();
        String sql ="UPDATE Clientes SET Nombre = ?, Apellido = ?, Telefono = ? WHERE Id = ?;";
        try {
            PreparedStatement pst = con.Conectar().prepareStatement(sql);
            pst.setString(1, getNombre());
            pst.setString(2, getApellido());
            pst.setString(3, getTelefono());
            pst.setInt(4, getId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public Boolean Eliminar(){
        Conexion con = new Conexion();
        String sql ="DELETE FROM Clientes WHERE Id = ?;";
        try {
            PreparedStatement pst = con.Conectar().prepareStatement(sql);
            pst.setInt(1, getId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public ArrayList<Clientes> Buscar(String txtBuscar){
        Conexion conn = new Conexion();
        String sql = "SELECT * FROM Clientes WHERE Nombre LIKE ( ? ) OR Apellido LIKE ( ? ) OR Telefono LIKE ( ? );";
        try {
            PreparedStatement pst = conn.Conectar().prepareStatement(sql);
            pst.setString(1, "%"+txtBuscar+"%");
            pst.setString(2, "%"+txtBuscar+"%");
            pst.setString(3, "%"+txtBuscar+"%");
            ResultSet rs = pst.executeQuery(sql);
            ArrayList<Clientes> arrClientes = new ArrayList<>();
            while(rs.next()){
                arrClientes.add(new Clientes(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getString("telefono")
                ));
            }
            return arrClientes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public ArrayList<Clientes> BuscarTodos(){
        Conexion conn = new Conexion();
        String sql = "Select * from Clientes";
        try {
            Statement st = conn.Conectar().createStatement();
            ResultSet rs = st.executeQuery(sql);
            ArrayList<Clientes> arrClientes = new ArrayList<>();
            while(rs.next()){
                arrClientes.add(new Clientes(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getString("telefono")
                ));
            }
            return arrClientes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public String toString(){
        return getNombre() + " - " + getApellido();
    }
}
