
package Modelo;

import Conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Productos {
    private int id;
    private String nombre;
    private int cantidad;
    private float precio;
    
    public Productos(){
        
    }

    public Productos(int id, String nombre, int cantidad, float precio) {
        this.id = id;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public Boolean Insertar(){
        Conexion con = new Conexion();
        String sql ="INSERT INTO Productos(id, Nombre, Cantidad, Precio) VALUES (null,?,?,?);";
        try {
            PreparedStatement pst = con.Conectar().prepareStatement(sql);
            pst.setString(1, getNombre());
            pst.setInt(2, getCantidad());
            pst.setFloat(3, getPrecio());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public Boolean Actualizar(){
        Conexion con = new Conexion();
        String sql ="UPDATE Productos SET Nombre = ?, Cantidad = ?, Precio = ? WHERE id = ?;";
        try {
            PreparedStatement pst = con.Conectar().prepareStatement(sql);
            pst.setString(1, getNombre());
            pst.setInt(2, getCantidad());
            pst.setFloat(3, getPrecio());
            pst.setInt(4, getId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public Boolean Eliminar(){
        Conexion con = new Conexion();
        String sql ="DELETE FROM Productos WHERE id = ?;";
        try {
            PreparedStatement pst = con.Conectar().prepareStatement(sql);
            pst.setInt(1, getId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public ArrayList<Productos> Buscar(String txtBuscar){
        Conexion conn = new Conexion();
        String sql = "SELECT * FROM Productos WHERE Nombre LIKE ( ? ) OR Cantidad LIKE ( ? ) OR Precio LIKE ( ? );";
        try {
            PreparedStatement pst = conn.Conectar().prepareStatement(sql);
            pst.setString(1, "%"+txtBuscar+"%");
            pst.setString(2, "%"+txtBuscar+"%");
            pst.setString(3, "%"+txtBuscar+"%");
            ResultSet rs = pst.executeQuery(sql);
            ArrayList<Productos> arrProductos = new ArrayList<>();
            while(rs.next()){
                arrProductos.add(new Productos(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getInt("cantidad"),
                        rs.getFloat("precio")
                ));
            }
            return arrProductos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<Productos> BuscarTodos(){
        Conexion conn = new Conexion();
        String sql = "Select * from Productos";
        try {
            Statement st = conn.Conectar().createStatement();
            ResultSet rs = st.executeQuery(sql);
            ArrayList<Productos> arrProductos = new ArrayList<>();
            while(rs.next()){
                arrProductos.add(new Productos(
                        rs.getInt("id"),
                        rs.getString("Nombre"),
                        rs.getInt("Cantidad"),
                        rs.getFloat("Precio")
                ));
            }
            return arrProductos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public String toString(){
        return getNombre();
    }
}
