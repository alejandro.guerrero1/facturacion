
package Modelo;

import Conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Date;
import java.sql.ResultSet;

public class Factura {
    private int id;
    private Date fecha;
    private int idCliente;
    
    public Factura(){
        
    }

    public Factura(int id, Date fecha, int idCliente) {
        this.id = id;
        this.fecha = fecha;
        this.idCliente = idCliente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    public int insertar(){
        Conexion conn = new Conexion();
        String sql = "insert into Facturas values(null, ?, ?);";
        
        try {
            PreparedStatement ps = conn.Conectar().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, getFecha());
            ps.setInt(2, getIdCliente());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()){
                return rs.getInt(1);
            } else {
                return 0;
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
