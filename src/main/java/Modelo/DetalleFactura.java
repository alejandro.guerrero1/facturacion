
package Modelo;

import Conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


public class DetalleFactura {
    private int id;
    private int cantidad;
    private float precioUnitario;
    private float importe;
    private int idProducto;
    private int idFactura;
    
    public DetalleFactura(){
        
    }

    public DetalleFactura(int id, int cantidad, float precioUnitario, float importe, int idProducto, int idFactura) {
        this.id = id;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.importe = importe;
        this.idProducto = idProducto;
        this.idFactura = idFactura;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }
    
    public void insertar(){
        Conexion conn = new Conexion();
        String sql = "INSERT INTO DetalleFacturas(Id, Cantidad, PrecioUnitario, Importe, IdProducto, IdFactura) VALUES (null,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.Conectar().prepareStatement(sql);
            ps.setInt(1, getCantidad());
            ps.setFloat(2, getPrecioUnitario());
            ps.setFloat(3, getImporte());
            ps.setInt(4, getIdProducto());
            ps.setInt(5, getIdFactura());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
